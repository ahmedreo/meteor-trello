import {Meteor} from 'meteor/meteor'
import {Accounts} from 'meteor/accounts-base'


// Styles
// import '/node_modules/tailwindcss/dist/tailwind.css'
import '/node_modules/tailwindcss/dist/utilities.css'
import '/imports/ui/sass/packages/board.scss'
import '/imports/ui/sass/main.scss'

// Start Your App
import Vue from 'vue'
import App from '/imports/ui/App.vue'
import router from '/imports/ui/router'
import store from '/imports/ui/store'
import VueMeteorTracker from 'vue-meteor-tracker'
import Vuebar from 'vuebar'
import Storage from 'vue-ls';

Vue.use(VueMeteorTracker)
Vue.use(Vuebar)
Vue.use(Storage)

// directives , filters and plugins
import '/imports/ui/directives'
import '/imports/ui/mixins'

Meteor.startup(() => {
	new Vue({
		el: '#app',
		router,
		store,
		render: (createElement) => {
			return createElement(App);
		}
	})

	Accounts.onLogout(() => {
		Vue.ls.remove('collapsed')
		Vue.ls.remove('sticky')
		store.commit('App/destroyStorage')
	})

})
