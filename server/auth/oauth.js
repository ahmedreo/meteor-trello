Accounts.onCreateUser(function (options, user) {
    
    // customize user
    function customize() {
        const generateUsername = (name) => {
            let hash = Math.floor(Math.random() * 100)
            let username = name.toLowerCase().split(' ').join('')
            username = username + hash
            if(Accounts.findUserByUsername(username)){
                generateUsername(username)
            }
            else {
                return username 
            }
        }
        const generateInitials = (name) => {
            return name.split(' ').map( i => i.charAt(0).toUpperCase()).join('')
        }
        user.username = generateUsername(options.profile.name)
        user.profile = options.profile
        user.profile.initials = generateInitials(options.profile.name)
    }

    // merge accounts services
    if (user.services.google) {
        
        let email = user.services.google.email
        let account = Accounts.findUserByEmail(email)

        if (account) {
            user = account
            Meteor.users.remove({email: user.email})
            return user
        }
        else{
            customize()
            user.emails = [
                {
                    address: email,
                    verified: true
                }
            ]
            return user
        }
    }
    else{
        customize()
        return user
    }
});