import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base'

// auth
import './auth/accounts.js'
import './auth/oauth.js'
import  './auth/mail.js'

// collections
import '/imports/collections/teams.js'
import { Teams } from  '/imports/collections/teams.js'
import '/imports/collections/boards.js'

Meteor.startup(() => {
	
	var Api = new Restivus({
    useDefaultAuth: false,
    defaultHeaders: {
    	// 'Content-Type': 'application/json',
    	'Access-Control-Allow-Origin': '*',
      // 'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS',
      // 'Access-Control-Allow-Headers': 'Content-Type, Authorization, X-Requested-With'
    }
    // enableCors: false,
    // prettyJson: true
  });
	
	Api.addRoute('teams' , {
    get: function () {
      return {
          statusCode: 200,
          headers: {
            "Content-Type": "text/json",
            "X-Custom-Header": "custom value"
          },
          body: {
            status: "success",
            data: Teams.find().fetch()
          }
        };
    },
    post: function(){
    	Teams.insert({
    		name: 'new',
    		description: 'asdsad',
    		owner_id: '107400622650877530515'
    	})
    	return {
          statusCode: 200,
          headers: {
            "Content-Type": "text/json",
            "X-Custom-Header": "custom value"
          },
          body: {
            status: "success",
            // data: Teams.find().fetch()
          }
        };
    }
  });

	Meteor.publish('users', function () {
		return Meteor.users.find()
	})

	Meteor.methods({
		updateUsername: function(username){
			var user = Accounts.setUsername(Meteor.userId() , username)
			return user;
		},
		setNewPassword: function(password){
			Accounts.setPassword(this.userId , password , {logout: false})
		}
	});
	
});
