export default {
	methods: {
    addClass() {
      let className = this.$route.path.substring(1).split('/')[0]
      document.body.className = className             
    },
    removeClass(){
      document.body.className = ''
    }
  },
  created() {
    this.addClass();
  },
  beforeDestroy(){
  	this.removeClass()
  }
}