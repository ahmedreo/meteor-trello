import Vue from 'vue'

const state = {
	stickyMode: false,
	createBoard: {
		isActive: false,
		team: {}
	}
}

const mutations = {
	mountStorage(state){
		state.stickyMode = Vue.ls.get('sticky')
	},
	destroyStorage(state){
		state.stickyMode = false
	},
	toggleCreateBoard(state , playload){
		state.createBoard.isActive = !state.createBoard.isActive
		playload ? state.createBoard.team = playload.team : state.createBoard.team = {}
	},
	toggleStickyMode(state){
		Vue.ls.set('sticky' , !state.stickyMode)
		state.stickyMode = !state.stickyMode
	}
}

const actions = {
	
}

export default {
	namespaced: true,
	state,
	mutations,
	actions
}