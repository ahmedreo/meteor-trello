import Vue from 'vue'

const invertImgs = (el , filter) => {
		el.childNodes.forEach((i) => {
			if (i.localName === 'img' || i.localName === 'svg') {
				i.style.filter = filter
			}
		})
}

Vue.directive('inverse' , (el , binding , vnode) => {
	let backgroundColor = el.style.backgroundColor
	backgroundColor = backgroundColor.substring(4, backgroundColor.length-1)
         .replace(/ /g, '')
         .split(',');
  
  let red = backgroundColor[0]
  let green = backgroundColor[1]
  let blue = backgroundColor[2]

  if (!binding.value) {
  	binding.value = []
  }

	let luma = (299 * red +  587 * green + 114 * blue)/1000; 
	if (luma > 135) {
		el.style.color = binding.value[0] || 'black'
		if (binding.modifiers.img) {
			invertImgs(el , 'invert(100%)')
		}
	}
	else {
		el.style.color = binding.value[1] || 'white'
		if (binding.modifiers.img) {
			invertImgs(el , '')
		}
	}
})