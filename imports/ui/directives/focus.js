import Vue from 'vue'

/* 
  v-focus is used to focus elements after mounting the component
  it's support all elements focusable and not focusable by adding tabindex attribute
  it's also used with dropdowns to focus links with arrow keys
  in case used with dropdown you could focus any children by index 
  you could use this directive in nested way
  if you want to style focused element use [v-focus-active] attribute
*/

const focusableElements = [
  'a:not([disabled])',
  'button:not([disabled])',
  'input[type=text]:not([disabled])',
  'textarea:not([disabled])',
  '[tabindex]:not([disabled]):not([tabindex="-1"])'
]

const query = focusableElements.join()

const getElements = parent => {
  return parent.querySelectorAll(query)
}

const getActiveIndex = elements => {
  return Array.prototype.indexOf.call(elements , document.activeElement)
}

const isFocusable = el => {
  let elements = getElements(document)
  return Array.prototype.includes.call(elements , el)
}

const focus = (el , binding) => {
  !isFocusable(el) && el.setAttribute('tabindex' , '0') 
  el.setAttribute('v-focus-active' , '')
  el.focus()
}

const onBlur = (el , binding) => {
  el.addEventListener('blur' , () => {
    el.removeAttribute('tabindex')
    el.removeAttribute('v-focus-active')
  })   
}

const focusWithDirection = (el , direction) => {
  let elements = getElements(el),
    activeIndex = getActiveIndex(elements)    


  if (direction == 'next') {
    elements[activeIndex + 1] && elements[activeIndex + 1].focus()
  }
  else if (direction == 'prev') {
    elements[activeIndex - 1] && elements[activeIndex - 1].focus()
  }
}

const prepareQueue = (el) => {
    el.setAttribute('v-focus-queue' , '')
    el.addEventListener('keyup' , e => {
      let isNested = el.querySelectorAll('[v-focus-queue]').length
      if (!isNested) {
        if (e.keyCode == 40) {
          focusWithDirection(el , 'next')
        }
        else if (e.keyCode == 38){
          focusWithDirection(el , 'prev')
        }
    }
  })
}

const focusQueue = (el , binding) => {
  let elements = getElements(el)
  if (binding.expression) {
    elements[binding.value].focus()
  }
  else {
    elements[0].focus()
  }
}

Vue.directive('focus', {
  inserted(el , binding , vnode) {
    el.trigger = document.activeElement
    
    if (binding.modifiers.queue) {
      prepareQueue(el)
      focusQueue(el , binding)
    }
    else {
      focus(el , binding)
      onBlur(el , binding)
    } 
  },
  unbind(el){
    let elements = getElements(document),
      activeIndex = getActiveIndex(elements)
    if (activeIndex == -1) {
      el.trigger.focus()
    }
  }
})