import Vue from 'vue'
import Router from 'vue-router'
import Welcome from '../views/Welcome'
import Goodbye from '../views/Goodbye'
import Dashboard from '../views/Dashboard'
import Login from '../views/auth/Login'
import Register from '../views/auth/Register'
import Set from '../views/auth/password/Set'
import Forget from '../views/auth/password/Forget'
import Reset from '../views/auth/password/Reset'
import Home from '../components/Home'
import Boards from '../components/Boards'
import Board from '../views/Board'

Vue.use(Router)

export default new Router({
	mode: 'history',
	routes: [
		{
			path: '/',
			component: Welcome,
		},
		{
			path: '/logged-out',
			component: Goodbye,
		},
		{
			path: '/dashboard',
			component: Dashboard,
			children: [
				{
					path: '',
					component: Home
				},
				{
					path: ':username/boards',
					component: Boards
				}
			]
		},
		{
			path: '/b/:id',
			component: Board
		},
		{
			path: '/login',
			component: Login
		},
		{
			path: '/register',
			component: Register
		},
		{
			path: '/set-password',
			component: Set
		},
		{
			path: '/forget-password',
			component: Forget
		},
		{
			path: '/reset-password/:token',
			component: Reset
		}
	]
})