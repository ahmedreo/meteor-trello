import { Mongo } from 'meteor/mongo'

export const Teams =  new Mongo.Collection('teams');


if (Meteor.isServer) {
	Meteor.publish('teams' , () => {
		// return Teams.find({owner_id : Meteor.userId()})
		return Teams.find()
	})
}

Meteor.methods({
	'teams.create'(data){
		Teams.insert(data)
	}
})