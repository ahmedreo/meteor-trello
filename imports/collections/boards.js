import { Mongo } from 'meteor/mongo'

export const Boards =  new Mongo.Collection('boards');

if (Meteor.isServer) {
	Meteor.publish('boards' , () => {
		return Boards.find({owner_id : Meteor.userId()})
	})
}

Meteor.methods({
	'boards.create'(data){
		Boards.insert(data)
	},
	'boards.toggleStar'(board){
		Boards.update(board._id , {$set : {starred: !board.starred}})
	}
})